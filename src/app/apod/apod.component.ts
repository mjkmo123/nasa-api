import { Component, OnInit } from '@angular/core';
import {ApodService} from "../services/apod.service";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-apod',
  templateUrl: './apod.component.html',
  styleUrls: ['./apod.component.sass']
})
export class ApodComponent implements OnInit {
  data:  any;
  loading = true;
  constructor(
    public apodService:ApodService
  ) { }

  ngOnInit(): void {
    this.getData();
  }
  getData(date = formatDate(new Date(), 'yyyy-MM-dd', 'en')): void{
    this.loading = true;
    this.apodService.get(date).subscribe((data) => {
      this.data = data;
      this.loading = false;
    },
      (error) => {
      this.data = {
        title: 'error',
        url: 'error',
        explanation: 'error',
        copyright: 'error',
        hdurl: ''
      }
      this.loading = false;
      });
  }
  setdate(date: string): void{
    date = formatDate(date, 'yyyy-MM-dd', 'en');
    this.getData(date);
  }
}
