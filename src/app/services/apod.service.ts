import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApodService {
  apiKey = 'kSzhmgdihJ5YIkZZRpDVItBF1vSsl8YfHhv9oSAb'
  constructor(
    private httpClient: HttpClient
  ) { }
  public get(date: string): Observable<any>{
    return this.httpClient.get('https://api.nasa.gov/planetary/apod?api_key=' + this.apiKey + '&date='  + date)
  }
}
