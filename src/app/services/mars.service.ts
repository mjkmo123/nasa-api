import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {formatDate} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class MarsService {
  apiKey = 'kSzhmgdihJ5YIkZZRpDVItBF1vSsl8YfHhv9oSAb';
  constructor(
    private httpClient: HttpClient
  ) { }
  public get(page: number, date, rover: string): Observable<any>{
    date = formatDate(date, 'yyyy-MM-dd', 'en');
    return this.httpClient.get('https://api.nasa.gov/mars-photos/api/v1/rovers/' + rover + '/photos?earth_date=' + date + '&api_key=' + this.apiKey + '&page=' + page);
  }
}
