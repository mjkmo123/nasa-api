import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApodComponent} from "./apod/apod.component";
import {MarsComponent} from "./mars/mars.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'apod', component: ApodComponent},
  {path: 'mars', component: MarsComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
