import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  dark = '/assets/theme/pink-bluegrey.css';
  light = '/assets/theme/indigo-pink.css';
  theme  = false;
  constructor(public router: Router) {
  }
  changeTheme(): void{
    this.theme = !this.theme;
    if (this.theme === true){
      // @ts-ignore
      document.getElementById('main-theme').href = this.dark;
    }else {
      // @ts-ignore
      document.getElementById('main-theme').href = this.light;
    }
  }
  home():void{
    this.router.navigate(['']);
  }
}
