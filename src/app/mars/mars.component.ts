import { Component, OnInit } from '@angular/core';
import {MarsService} from "../services/mars.service";
import {formatDate} from "@angular/common";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-mars',
  templateUrl: './mars.component.html',
  styleUrls: ['./mars.component.sass']
})
export class MarsComponent implements OnInit {
  page = 1;
  loading = true;
  data = [];
  date = formatDate(new Date(), 'MM/dd/yyyy', 'en');
  rover = new FormControl('curiosity');
  constructor(private marsService: MarsService) { }

  ngOnInit(): void {
    this.rover.setValue('curiosity');
    this.get();
  }
  get(page = 1, date = new Date(), rover = this.rover.value): void{
    this.loading = true;
    this.marsService.get(page, date, rover).subscribe((data) => {
      this.data = data['photos'];
      this.loading = false;
    },
      (error) => {
      this.loading = false;
      this.data = [];
      });
  }

  changePage(count): void{
    this.page += count;
    this.get(this.page, new Date(this.date), this.rover.value);
  }

  changeDate(date): void{
    this.page = 1;
    this.date = date;
    this.get(this.page, new Date(date), this.rover.value);
  }
}
